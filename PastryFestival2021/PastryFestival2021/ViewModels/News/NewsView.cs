﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PastryFestival2021.ViewModels.News
{
    public class NewsIndexView
    {
        public IEnumerable<NewsView> NewsList { get; set; }
    }
    public class NewsView
    {
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "類型")]
        public int Type { get; set; }

        [Display(Name = "新聞圖片")]
        public string Image { get; set; }

        [Display(Name = "內容")]
        public string Content { get; set; }

        [Display(Name = "上線時間")]
        public System.DateTime OnlineTime { get; set; }
    }
}