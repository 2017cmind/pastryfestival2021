﻿using PastryFestival2021.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PastryFestival2021.ViewModels.Store
{
    public class StoreIndexView
    {
        public IEnumerable<StoreView> List { get; set; }
        [Display(Name = "店家名稱")]
        public string searchText { get; set; }

        [Display(Name = "區域類型")]
        public int Type { get; set; }
    }
    public class StoreView
    {
        public int ID { get; set; }

        [Display(Name = "店家名稱")]
        public string Name { get; set; }

        [Display(Name = "區域類型")]
        public int Type { get; set; }

        [Display(Name = "獎項項目")]
        public AwardsEnum Awards { get; set; }

        [Display(Name = "地址")]
        public string Address { get; set; }

        [Display(Name = "圖片")]
        public string Logo { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }

        [Display(Name = "圖片")]
        public string Image2 { get; set; }

        [Display(Name = "影片")]
        public string VideoUrl { get; set; }

        [Display(Name = "店家介紹")]
        public string Info { get; set; }

        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Display(Name = "獲獎產品")]
        public string ProductName { get; set; }

        [Display(Name = "產品介紹")]
        public string ProductInfo { get; set; }

        [Display(Name = "店家連結")]
        public string Link { get; set; }
    }
}