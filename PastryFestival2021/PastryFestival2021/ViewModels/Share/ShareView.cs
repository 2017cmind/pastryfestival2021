﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PastryFestival2021.ViewModels.Share
{
    public class ShareView
    {
        public string BaseSixtyFour
        {
            get;
            set;
        }

        public string CardName
        {
            get;
            set;
        }

        [Required]
        public string Draw
        {
            get;
            set;
        }

        [Required]
        public string DrawDesc
        {
            get;
            set;
        }

        [Required]
        public string Email
        {
            get;
            set;
        }

        [Required]
        public int ID
        {
            get;
            set;
        }

        public string LoginEmail
        {
            get;
            set;
        }

        public string LoginName
        {
            get;
            set;
        }

        public string LoginPhone
        {
            get;
            set;
        }

        public string LoginType
        {
            get;
            set;
        }

        [Required]
        public string Name
        {
            get;
            set;
        }

        [Required]
        public string Phone
        {
            get;
            set;
        }

        public string Photo
        {
            get;
            set;
        }

        [Required]
        public string UserId
        {
            get;
            set;
        }

        [Required]
        public string WebDesc
        {
            get;
            set;
        }

        public string Wish
        {
            get;
            set;
        }
    }
}