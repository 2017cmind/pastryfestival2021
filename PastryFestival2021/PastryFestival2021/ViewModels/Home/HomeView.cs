﻿using PastryFestival2021.ViewModels.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PastryFestival2021.ViewModels.Home
{
    public class HomeView
    {
        public IEnumerable<NewsView> NewsList { get; set; }
        public int? Vote1Count { get; set; }
        public int? Vote2Count { get; set; }
        public int? Vote3Count { get; set; }
    }
}