﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PastryFestival2021.ViewModels.Member
{
    public class MemberView
    {
        [Required]
        [StringLength(125, ErrorMessage = "{0}不能超過{1}字. ")]
        [RegularExpression(@"^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$", ErrorMessage = "Email格式錯誤 範例: AAA@gmail.com！")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "{0}不能超過{1}字. ")]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "{0}不能超過{1}字. ")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "電話只能輸入數字.")]
        [Display(Name = "電話")]
        public string Phone { get; set; }

        public string FBId { get; set; }

        public int ID { get; set; }
    }
}