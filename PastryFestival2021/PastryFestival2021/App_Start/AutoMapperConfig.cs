﻿using AutoMapper;
using PastryFestival2021.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PastryFestival2021.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                #region 後台
                cfg.CreateMap<News, Areas.Admin.ViewModels.News.NewsView>();
                cfg.CreateMap<Areas.Admin.ViewModels.News.NewsView, News>();

                cfg.CreateMap<Store, Areas.Admin.ViewModels.Store.StoreView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Store.StoreView, Store>();

                cfg.CreateMap<VoteLog, Areas.Admin.ViewModels.Vote.VoteView > ();

                #endregion
                #region 前台
                cfg.CreateMap<Member, ViewModels.Member.MemberView>();
                cfg.CreateMap<ViewModels.Member.MemberView, Member>();

                cfg.CreateMap<News, ViewModels.News.NewsView>();
                cfg.CreateMap<Store, ViewModels.Store.StoreView>();
                #endregion
            });
        }
    }
}