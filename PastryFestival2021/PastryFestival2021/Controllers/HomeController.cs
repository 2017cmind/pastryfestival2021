﻿using AutoMapper;
using PastryFestival2021.ActionFilters;
using PastryFestival2021.Models;
using PastryFestival2021.Models.Cmind;
using PastryFestival2021.Repositories;
using PastryFestival2021.Utility.Cmind;
using PastryFestival2021.ViewModels.Home;
using PastryFestival2021.ViewModels.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using PastryFestival2021.ViewModels.News;

namespace PastryFestival2021.Controllers
{
    public class HomeController : BaseController
    {
        private VoteLogRepository votelogRepository = new VoteLogRepository();
        private MemberRepository memberRepository = new MemberRepository();
        private NewsRepository newsRepository = new NewsRepository();


        public ActionResult Index(HomeView model)
        {
            var newsquery = newsRepository.List();
            if(newsquery.Count() > 0)
            {
                newsquery = newsRepository.List().Take(5);
            }
            model.NewsList = Mapper.Map<IEnumerable<NewsView>>(newsquery);
            model.Vote1Count = votelogRepository.Query(1)?.Count();
            model.Vote2Count = votelogRepository.Query(2)?.Count();
            model.Vote3Count = votelogRepository.Query(3)?.Count();

            return View(model);
        }

        public JsonResult Voted(int vid = 0)
        {
            MemberInfo memberInfo = MemberInfoHelper.GetMemberInfo();
            var result = "";
            if (memberInfo != null) {
                var member = memberRepository.SocialLogin(memberInfo.ID);
                if (member == null)
                {
                    result = "form";
                }
                else {
                    var query = votelogRepository.GetVoteLog(memberInfo.ID);
                    if (query == null)
                    {
                        votelogRepository.Insert(vid: vid, fid: memberInfo.ID, name: member.Name);
                    }
                    //測試
                    //result = "success";

                    result = query == null ? "success" : "voted";
                }
            }
            else
            {
                result = "error";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [MemberAuthorize]
        public ActionResult Form()
        {
            var model = new MemberView();
            MemberInfo memberInfo = MemberInfoHelper.GetMemberInfo();
            var query = memberRepository.SocialLogin(memberInfo.ID);
            if (query != null)
            {
                model = Mapper.Map<MemberView>(query);
                model.ID = query.ID;
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(MemberView model)
        {
            var vid = Session["Vid"]?.ToString();
            MemberInfo memberInfo = MemberInfoHelper.GetMemberInfo();
            Session["Vid"] = null;
            if (memberInfo != null) {
                if (ModelState.IsValid)
                {
                    model.FBId = memberInfo.ID;
                    Member data = Mapper.Map<Member>(model);
                    if (model.ID == 0)
                    {
                        model.ID = memberRepository.Insert(data);                    
                    }
                    else
                    {
                        memberRepository.Update(data);
                    }
                    var query = votelogRepository.GetVoteLog(memberInfo.ID);
                    if (query == null)
                    {
                        int Vid = vid != null ? Convert.ToInt32(vid) : 0;
                        if (Vid != 0)
                        {
                            votelogRepository.Insert(vid: Vid, fid: memberInfo.ID, name: data.Name);
                            ShowMessage(true, "投票成功");
                        }
                    }
                    else
                    {
                        ShowMessage(false, "今日已投票，請明日再投");

                    }
                    return RedirectToAction("Index", "Home");
                }
            }
            
            return View(model);
        }

    }
}