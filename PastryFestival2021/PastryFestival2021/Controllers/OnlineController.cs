﻿using PastryFestival2021.ViewModels.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PastryFestival2021.Repositories;
using Newtonsoft.Json;

namespace PastryFestival2021.Controllers
{
    public class OnlineController : BaseController
    {
        private StoreRepository storeRepository = new StoreRepository();
        // GET: Online
        public ActionResult Index(StoreIndexView model)
        {
            var query = storeRepository.Query(model.searchText, model.Type, 0, "1");
            model.List = AutoMapper.Mapper.Map<IEnumerable<StoreView>>(query);
            return View(model);
        }

        public JsonResult Search(string title, int type)
        {
            var query = storeRepository.Query(title, type, 0, "1");
            var result = JsonConvert.SerializeObject(query.ToArray());

            return Json(result, JsonRequestBehavior.AllowGet);

        }
    }
}