﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using PastryFestival2021.Repositories;
using PastryFestival2021.ViewModels.News;

namespace PastryFestival2021.Controllers
{
    public class NewsController : BaseController
    {
        private NewsRepository newsRepository = new NewsRepository();
        // GET: News
        public ActionResult Index(NewsIndexView model)
        {
            var query = newsRepository.List();
            model.NewsList = Mapper.Map<IEnumerable<NewsView>>(query);
            return View(model);
        }
        public ActionResult Detail(int id)
        {
            var model = new NewsView();
            if (id != 0) {
                var query = newsRepository.GetById(id);
                if (query != null)
                {
                    model = Mapper.Map<NewsView>(query);
                }
                else {
                    RedirectToAction("Index", "Home");
                }
            }
            return View(model);
        }
    }
}