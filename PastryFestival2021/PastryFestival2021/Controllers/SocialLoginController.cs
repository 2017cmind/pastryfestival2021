﻿using Newtonsoft.Json;
using NLog;
using PastryFestival2021.Models;
using PastryFestival2021.Models.Cmind;
using PastryFestival2021.Repositories;
using PastryFestival2021.Utility;
using PastryFestival2021.Utility.Cmind;
using PastryFestival2021.ViewModels.Member;
using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;

namespace PastryFestival2021.Controllers
{
    public class SocialLoginController : BaseController
    {
        private MemberRepository memberRepository = new MemberRepository();
        private readonly string FB_APP_ID = ConfigurationManager.AppSettings["FB_APP_ID"];
        private readonly string FB_APP_SECRET = ConfigurationManager.AppSettings["FB_APP_SECRET"];
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 
        /// </summary>
        /// <returns>
        /// /SocialLogin/FacebookCallback
        /// </returns>
        private string getFBRedirectUrl()
        {
            string redirectUrl = string.Format("{0}://{1}/{2}/{3}", HttpContext.Request.Url.Scheme, HttpContext.Request.Url.Authority, "SocialLogin", "FacebookCallback");
            return redirectUrl;
        }

        [ValidateInput(false)]
        public ActionResult FBRegister(int vid)
        {
            Session["Vid"] = vid;
            string fbRedirectUrl = getFBRedirectUrl();
            string url = FacebookCallbackUtility.GetLoginUrl(FB_APP_ID, "email", fbRedirectUrl);
            return Redirect(url);
        }

        public ActionResult FacebookCallback(string code)
        {

            string fbRedirectUrl = getFBRedirectUrl();
            var token = FacebookCallbackUtility.GetTokenFromCode(
                    code,
                    FB_APP_ID,
                    FB_APP_SECRET,
                    fbRedirectUrl);

            var userInfoResult = FacebookCallbackUtility.GetUserInfo(token.access_token, "email");
            MemberInfo memberInfo = new MemberInfo()
            {
                ID = userInfoResult.id
            };
            var member = memberRepository.SocialLogin(userInfoResult.id);
            if (member != null) {
                logger.Info(string.Format("已經有FB註冊:{0}", userInfoResult.id));
            }
            else
            {
                logger.Info(string.Format("尚未註冊，FB UserId {0}", userInfoResult.id));
            }
            //FB登入
            MemberInfoHelper.Login(memberInfo, false);

            return RedirectToAction("Form", "Home");
        }
    }
}