﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PastryFestival2021.Utility.Cmind
{
    public class ImageHelper
    {
        /// <summary>
        /// 儲存圖片
        /// </summary>
        /// <param name="photoFile">
        /// 要儲存的檔案
        /// </param>
        /// <param name="savePath">
        /// 儲存的路徑
        /// </param>
        /// /// <param name="fileName">
        /// 檔名，若輸入空字串則為yyyyMMddhhmmssfff
        /// </param>
        /// <returns>
        /// 儲存結果，
        /// 成功時Message為檔案名稱，若失敗則會回傳錯誤訊息
        /// </returns>
        public static string SaveImage(string savePath, HttpPostedFileBase file, string fileName = "")
        {
            string extension = Path.GetExtension(file.FileName);
            if (string.IsNullOrEmpty(fileName))
            {
                fileName = GenerateFileName();
            }
            string fileNameWithExtension = string.Format("{0}{1}", fileName, extension);
            string path = Path.Combine(savePath, fileNameWithExtension);
            file.SaveAs(path);
            return fileNameWithExtension;
        }

        /// <summary>
        /// 檢查圖片是否存在
        /// </summary>
        /// <param name="photoFile"></param>
        /// <returns></returns>
        public static bool CheckFileExists(HttpPostedFileBase photoFile)
        {
            if (photoFile == null)
            {
                return false;
            }
            if (photoFile.ContentLength == 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 刪除檔案
        /// </summary>
        /// <param name="filename">檔名</param>
        /// <param name="path">路徑</param>
        public static void DeletePhoto(string path, string filename)
        {
            filename = filename ?? "";
            string ComBinePath = Path.Combine(path, filename);

            if (File.Exists(ComBinePath))
            {
                File.Delete(ComBinePath);
            }
        }

        public static string GenerateFileName()
        {
            return DateTime.Now.ToString("yyyyMMddhhmmssfff");
        }

    }
}