﻿using Facebook;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace PastryFestival2021.Utility
{
    /// <summary>
    /// 參考文件：https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow#checktoken
    /// </summary>
    public class FacebookCallbackUtility
    {
        private const string AUTH_API = "https://www.facebook.com/v11.0/dialog/oauth?client_id={0}&scope={1}&redirect_uri={2}";

        /// <summary>
        /// 
        /// </summary>
        /// <param name = "clientId" ></ param >
        /// < param name="scope">
        /// 取得權限範圍
        /// </param>
        /// <param name = "redirectUri" >
        /// 回傳網址
        /// </ param >
        /// < param name="state">
        /// 驗證用
        /// </param>
        ///<returns></returns>
        public static string GetLoginUrl(string clientId, string scope, string redirectUri)
        {
            redirectUri = HttpUtility.UrlEncode(redirectUri);
            return string.Format(AUTH_API, clientId, scope, redirectUri);
        }

        public static GetTokenFromCodeResult GetTokenFromCode(string code, string client_id, string client_secret, string redirect_uri)
        {
            try
            {
                var fb = new FacebookClient();
                dynamic token = fb.Post("oauth/access_token", new
                {
                    client_id = client_id,
                    client_secret = client_secret,
                    redirect_uri = redirect_uri,
                    code = code
                });

                GetTokenFromCodeResult result = JsonConvert.DeserializeObject<GetTokenFromCodeResult>(token.ToString());
                return result;
            }
            catch (WebException ex)
            {
                using (var reader = new System.IO.StreamReader(ex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    throw new Exception("GetTokenFromCode: " + responseText, ex);
                }
            }
        }

        public static UserInfoResult GetUserInfo(string accessToken, string scope)
        {
            try
            {
                var fb = new FacebookClient();
                fb.AccessToken = accessToken;
                string path = string.Format("me?fields={0}", scope);
                dynamic me = fb.Get(path);

                UserInfoResult result = JsonConvert.DeserializeObject<UserInfoResult>(me.ToString());
                return result;
            }
            catch (WebException ex)
            {
                using (var reader = new System.IO.StreamReader(ex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    throw new Exception("GetUserInfo: " + responseText, ex);
                }
            }
        }

        public class GetTokenFromCodeResult
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public int expires_in { get; set; }
        }

        public class UserInfoResult
        {
            public string id { get; set; }
            public string email { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string name { get; set; }
        }
    }
}