﻿using Newtonsoft.Json;
using PastryFestival2021.Models.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PastryFestival2021.Utility.Cmind
{
    public class MemberInfoHelper
    {
        /// <summary>
        /// 存放資料的Key
        /// </summary>
        private const string CMIND_MEMBER = "PastryFestival2021Member";

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="memberInfo"></param>
        /// <param name="isRemeber"></param>
        public static void Login(MemberInfo memberInfo, bool isRemeber)
        {
            string data = JsonConvert.SerializeObject(memberInfo);

            if (isRemeber)
            {
                DateTime expires = DateTime.Now.AddDays(30);
                CookieHelper.SetCookie(CMIND_MEMBER, data, expires);
            }
            else
            {
                SessionHelper.SetSession(CMIND_MEMBER, data);
            }
        }

        /// <summary>
        /// 登出
        /// </summary>
        public static void Logout()
        {
            SessionHelper.Del(CMIND_MEMBER);
            CookieHelper.Del(CMIND_MEMBER);
        }

        /// <summary>
        /// 取得會員資料
        /// </summary>
        /// <returns></returns>
        public static MemberInfo GetMemberInfo()
        {
            string data = SessionHelper.Get(CMIND_MEMBER);
            if (!string.IsNullOrEmpty(data))
            {
                MemberInfo memberInfo = JsonConvert.DeserializeObject<MemberInfo>(data);
                return memberInfo;
            }

            data = CookieHelper.Get(CMIND_MEMBER);
            if (!string.IsNullOrEmpty(data))
            {
                MemberInfo memberInfo = JsonConvert.DeserializeObject<MemberInfo>(data);
                return memberInfo;
            }
            return null;
        }

        /// <summary>
        /// 會員是否為登入狀態
        /// </summary>
        /// <returns></returns>
        public static bool IsLogin()
        {
            MemberInfo memberInfo = GetMemberInfo();
            return memberInfo != null;
        }
    }
}