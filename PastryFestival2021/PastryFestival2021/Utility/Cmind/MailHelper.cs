﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Serialization;

namespace PastryFestival2021.Utility.Cmind
{
    public class MailHelper
    {
        private static readonly string senderMail = ConfigurationManager.AppSettings["SenderMail"];
        private static readonly string senderDisplayName = ConfigurationManager.AppSettings["SenderDisplayName"];

        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static bool SendEmail(MailBase mailBase)
        {
            MailAddress from;
            if (!string.IsNullOrEmpty(senderDisplayName))
            {
                from = new MailAddress(senderMail, senderDisplayName, System.Text.Encoding.UTF8);
            }
            else
            {
                from = new MailAddress(senderMail);
            }

            SmtpClient smtpMail = new SmtpClient();
            smtpMail.EnableSsl = false;

            bool sendMailResult = sendEmail(smtpMail, from, mailBase);
            logger.Info(string.Format("Send Mail Result: {0}, mail: {1}",
                sendMailResult, mailBase.Email));
            return sendMailResult;
        }

        private static bool sendEmail(SmtpClient client, MailAddress from, MailBase mailBase)
        {
            bool result = true;
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = from;
                string[] mail = mailBase.Email.Trim().Split(",".ToCharArray());
                foreach (string reciver in mail)
                {
                    mailMessage.To.Add(reciver.Trim());
                }
                mailMessage.Subject = mailBase.Subject;
                System.Text.StringBuilder strBody = new System.Text.StringBuilder();
                strBody.Append(mailBase.MailBody);
                mailMessage.Body = strBody.ToString();
                mailMessage.IsBodyHtml = true;
                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                var json = new JavaScriptSerializer().Serialize(mailBase);
                string msg = string.Format(
                    "Send Mail False, mail:{0}, errorMsg:{1}",
                    json,
                    ex.Message);
                logger.Error(msg);
                result = false;
            }
            return result;
        }
    }

    public class MailBase
    {
        public virtual string Email { get; set; }

        public virtual string Subject { get; set; }

        public virtual string MailBody { get; set; }
    }
}