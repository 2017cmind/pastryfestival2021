﻿using PastryFestival2021.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PastryFestival2021.ActionFilters
{
    /// <summary>
    /// 前台會員登入驗證
    /// </summary>
    public class MemberAuthorize : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool isLogin = MemberInfoHelper.IsLogin();
            if (!isLogin)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        { "controller", "Home" },
                        { "action", "Index" }
                    });
            }
            base.OnActionExecuting(filterContext);
        }
    }
}