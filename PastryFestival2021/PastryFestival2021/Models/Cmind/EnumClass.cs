﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace PastryFestival2021.Models.Cmind
{

    /// <summary>
    /// 分區
    /// </summary>
    public enum ZoneEnum
    {
        /// <summary>
        /// 北區
        /// </summary>
        [Description("北區")]
        North = 1,

        /// <summary>
        /// 中區       
        /// </summary>
        [Description("中區")]
        Central = 2,
        /// <summary>
        /// 南區       
        /// </summary>
        [Description("南區")]
        South = 3,
    }
    /// <summary>
    /// 獎項類別
    /// </summary>
    public enum AwardsEnum
    {
        /// <summary>
        /// 110年臺灣餅 傳承獎
        /// </summary>
        [Description("110年臺灣餅【傳承獎】")]
        Awards1 = 1,

        /// <summary>
        /// 110年臺灣餅 創新獎       
        /// </summary>
        [Description("110年臺灣餅【創新獎】")]
        Awards2 = 2,
        /// <summary>
        /// 110年臺灣餅 傳承/創新獎       
        /// </summary>
        [Description("110年臺灣餅【傳承獎/創新獎】")]
        Awards3 = 3,
    }
}