﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PastryFestival2021.Models.Cmind
{
    public class MemberInfo
    {
        public string ID { get; set; }

        public string Account { get; set; }
    }
}