﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PastryFestival2021.Models.Cmind
{
    public class PageResult<T> : Page
    {
        public IEnumerable<T> Data { get; set; }
    }
}