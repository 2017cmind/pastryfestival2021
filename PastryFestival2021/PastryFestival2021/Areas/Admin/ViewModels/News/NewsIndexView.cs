﻿using PastryFestival2021.Models.Cmind;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PastryFestival2021.Areas.Admin.ViewModels.News
{
    public class NewsIndexView : PageQuery
    {
        public NewsIndexView()
        {
            this.Sorting = "OnlineTime";
            this.IsDescending = true;
        }

        public PageResult<NewsView> PageResult { get; set; }

        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "上線時間")]
        public System.DateTime OnlineTime { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }
    }
}