﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace PastryFestival2021.Areas.Admin.ViewModels.News
{
    public class NewsView
    {
        public int ID { get; set; }

        [Required]
        [StringLength(125, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "新聞圖片")]
        public string Image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }

        [AllowHtml]
        [Display(Name = "內容")]
        public string Content { get; set; }

        [Required]
        [Display(Name = "上線時間")]
        public System.DateTime OnlineTime { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }
    }
}