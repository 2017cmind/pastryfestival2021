﻿using PastryFestival2021.Models.Cmind;
using PastryFestival2021.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PastryFestival2021.Areas.Admin.ViewModels.Store
{
    public class StoreView
    {
        public int ID { get; set; }
        
        [Required]
        [StringLength(25, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "店家名稱")]
        public string Name { get; set; }

        [Display(Name = "區域類型")]
        public int Type { get; set; }

        public List<SelectListItem> TypeOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ZoneEnum>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        [StringLength(50, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "地址")]
        public string Address { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "圖片2")]
        public string Image2 { get; set; }
        public HttpPostedFileBase Image2File { get; set; }

        [StringLength(125, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "影片")]
        public string VideoUrl { get; set; }

        [StringLength(250, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "店家介紹")]
        public string Info { get; set; }

        [Display(Name = "電話")]
        public string Phone { get; set; }


        [Display(Name = "獎項")]
        public int Awards { get; set; }

        public List<SelectListItem> AwardsOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<AwardsEnum>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        [Required]
        [StringLength(15, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "獲獎產品")]
        public string ProductName { get; set; }

        [StringLength(250, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "產品介紹")]
        public string ProductInfo { get; set; }

        [StringLength(125, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "店家連結")]
        public string Link { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }
    }
}