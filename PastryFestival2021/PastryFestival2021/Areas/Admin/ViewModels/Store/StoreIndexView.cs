﻿using PastryFestival2021.Models.Cmind;
using PastryFestival2021.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PastryFestival2021.Areas.Admin.ViewModels.Store
{
    public class StoreIndexView : PageQuery
    {
        public StoreIndexView()
        {
            this.Sorting = "Sort";
            this.IsDescending = false;
        }
        public PageResult<StoreView> PageResult { get; set; }
        public int ID { get; set; }

        [Display(Name = "店家名稱")]
        public string Name { get; set; }

        [Display(Name = "區域分類")]
        public int Type { get; set; }

        public List<SelectListItem> TypeOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ZoneEnum>();
                result.Insert(0, new SelectListItem { Text = "全部", Value = "0" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        [Display(Name = "獎項")]
        public int Awards { get; set; }

        public List<SelectListItem> AwardsOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<AwardsEnum>();
                result.Insert(0, new SelectListItem { Text = "全部", Value = "0" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "地址")]
        public string Address { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }

        [Display(Name = "影片")]
        public string VideoUrl { get; set; }

        [Display(Name = "店家介紹")]
        public string Info { get; set; }

        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Display(Name = "獲獎產品")]
        public string ProductName { get; set; }

        [Display(Name = "產品介紹")]
        public string ProductInfo { get; set; }

        [Display(Name = "店家連結")]
        public string Link { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }
    }
}