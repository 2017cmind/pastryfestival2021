﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PastryFestival2021.Areas.Admin.ViewModels.Vote
{
    public class VoteView
    {
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string FBId { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "投票口罩")]
        public int VoteId { get; set; }

        [Display(Name = "建立日期")]
        public System.DateTime CreateTime { get; set; }
    }
}