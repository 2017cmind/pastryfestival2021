﻿using AutoMapper;
using PastryFestival2021.ActionFilters;
using PastryFestival2021.Areas.Admin.ViewModels.Store;
using PastryFestival2021.Models;
using PastryFestival2021.Models.Cmind;
using PastryFestival2021.Repositories;
using PastryFestival2021.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PastryFestival2021.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class StoreAdminController : BaseAdminController
    {
        // GET: Admin/StoreAdmin
        private StoreRepository storeRepository = new StoreRepository();
        public ActionResult Index(StoreIndexView model)
        {
            var query = storeRepository.Query(model.Name, model.Type, model.Awards);
            var pageResult = query.ToPageResult<Store>(model);
            model.PageResult = Mapper.Map<PageResult<StoreView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            StoreView model;
            if (id == 0)
            {
                model = new StoreView();
            }
            else
            {
                var query = storeRepository.GetById(id);
                model = Mapper.Map<StoreView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(StoreView model)
        {
            bool ImagehasFile = ImageHelper.CheckFileExists(model.ImageFile);
            bool Image2hasFile = ImageHelper.CheckFileExists(model.Image2File);

            if (ModelState.IsValid)
            {
                if (ImagehasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, model.Image);
                    model.Image = ImageHelper.SaveImage(PhotoFolder, model.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-image1");
                }

                if (Image2hasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, model.Image2);
                    model.Image2 = ImageHelper.SaveImage(PhotoFolder, model.Image2File, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-image2");
                }

                Store data = Mapper.Map<Store>(model);
                data.UpdateTime = DateTime.Now;
                data.Updater = AdminInfoHelper.GetAdminInfo().ID;
                if (model.ID == 0)
                {
                    data.CreateTime = DateTime.Now;
                    data.Creater = AdminInfoHelper.GetAdminInfo().ID;
                    model.ID = storeRepository.Insert(data);
                }
                else
                {
                    storeRepository.Update(data);
                }

                ShowMessage(true, "儲存成功");
                return RedirectToAction(nameof(StoreAdminController.Edit), new { id = model.ID });
            }
            else
            {
                ShowMessage(false, "輸入資料有誤");
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            DeleteImage(id, "Image");
            DeleteImage(id, "Logo");
            DeleteImage(id, "Image2");
            storeRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction(nameof(StoreAdminController.Index));
        }

        public ActionResult DeleteImage(int id, string type)
        {
            var image = storeRepository.DeleteImage(id, type);
            ImageHelper.DeletePhoto(PhotoFolder, image);
            //刪除圖片
            ShowMessage(true, "刪除圖片成功");
            return RedirectToAction(nameof(StoreAdminController.Edit), new { id = id });
        }
    }
}