﻿using AutoMapper;
using PastryFestival2021.ActionFilters;
using PastryFestival2021.Areas.Admin.ViewModels.Vote;
using PastryFestival2021.Models;
using PastryFestival2021.Models.Cmind;
using PastryFestival2021.Repositories;
using PastryFestival2021.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PastryFestival2021.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class VoteAdminController : BaseAdminController
    {
        private VoteLogRepository votelogRepository = new VoteLogRepository();
        // GET: Admin/VoteAdmin
        public ActionResult Index(VoteIndexView model)
        {
            var query = votelogRepository.GetAll();
            var pageResult = query.ToPageResult<VoteLog>(model);
            model.PageResult = Mapper.Map<PageResult<VoteView>>(pageResult);
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            votelogRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction(nameof(VoteAdminController.Index));
        }
    }
}