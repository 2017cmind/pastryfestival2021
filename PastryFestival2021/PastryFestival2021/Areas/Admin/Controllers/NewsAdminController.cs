﻿using AutoMapper;
using PastryFestival2021.ActionFilters;
using PastryFestival2021.Areas.Admin.ViewModels.News;
using PastryFestival2021.Models;
using PastryFestival2021.Models.Cmind;
using PastryFestival2021.Repositories;
using PastryFestival2021.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PastryFestival2021.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class NewsAdminController : BaseAdminController
    {
        private NewsRepository newsRepository = new NewsRepository();

        // GET: Admin/NewsAdmin
        public ActionResult Index(NewsIndexView model)
        {
            var query = newsRepository.Query(model.Title);
            var pageResult = query.ToPageResult<News>(model);
            model.PageResult = Mapper.Map<PageResult<NewsView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            NewsView model;
            if (id == 0)
            {
                model = new NewsView();
                model.OnlineTime = DateTime.Now;
            }
            else
            {
                var query = newsRepository.GetById(id);
                model = Mapper.Map<NewsView>(query);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NewsView model)
        {
            bool hasFile = ImageHelper.CheckFileExists(model.ImageFile);
            if (ModelState.IsValid)
            {
                if (hasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, model.Image);
                    model.Image = ImageHelper.SaveImage(PhotoFolder, model.ImageFile);
                }

                News data = Mapper.Map<News>(model);
                data.UpdateTime = DateTime.Now;
                data.Updater = AdminInfoHelper.GetAdminInfo().ID;
                if (model.ID == 0)
                {
                    data.CreateTime = DateTime.Now;
                    data.Creater = AdminInfoHelper.GetAdminInfo().ID;
                    model.ID = newsRepository.Insert(data);
                }
                else
                {
                    newsRepository.Update(data);
                }

                ShowMessage(true, "儲存成功");
                return RedirectToAction(nameof(NewsAdminController.Edit), new { id = model.ID });
            }
            else
            {

                ShowMessage(false, "輸入資料有誤");
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            DeleteImage(id);
            newsRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction(nameof(NewsAdminController.Index));
        }

        public ActionResult DeleteImage(int id)
        {
            var image = newsRepository.DeleteImage(id);
            //刪除圖片
            ImageHelper.DeletePhoto(PhotoFolder, image);
            ShowMessage(true, "刪除圖片成功");
            return RedirectToAction(nameof(NewsAdminController.Edit), new { id = id });
        }
    }
}