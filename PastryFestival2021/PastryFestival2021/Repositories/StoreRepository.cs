﻿using PastryFestival2021.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PastryFestival2021.Repositories
{
    public class StoreRepository
    {
        private PastryFestival2021DBEntities db = new PastryFestival2021DBEntities();

        public void Update(Store oldData)
        {
            Store data = db.Store.Find(oldData.ID);
            data.Name = oldData.Name;
            data.Phone = oldData.Phone;
            data.ProductInfo = oldData.ProductInfo;
            data.Sort = oldData.Sort;
            data.Address = oldData.Address;
            data.Image = oldData.Image;
            data.Image2 = oldData.Image2;
            data.Info = oldData.Info;
            data.Link = oldData.Link;
            data.ProductName = oldData.ProductName;
            data.Awards = oldData.Awards;
            data.Status = oldData.Status;
            data.Type = oldData.Type;
            data.VideoUrl = oldData.VideoUrl;
            data.Updater = oldData.Updater;
            data.UpdateTime = DateTime.Now;
            db.SaveChanges();
        }

        public int Insert(Store data)
        {
            db.Store.Add(data);
            db.SaveChanges();
            return data.ID;
        }

        public IQueryable<Store> Query(string name, int type, int awardstype, string status = null)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.Name.Contains(name));
            }
            if (!string.IsNullOrEmpty(status))
            {
                bool statusbool = status == "1";
                query = query.Where(p => p.Status == statusbool);
            }
            if (type != 0)
            {
                query = query.Where(p => p.Type == type);
            }
            if (awardstype != 0)
            {
                query = query.Where(p => p.Awards == awardstype);
            }
            return query;
        }

        public IQueryable<Store> GetAll()
        {
            var query = db.Store;
            return query;
        }

        public Store GetById(int id)
        {
            var query = db.Store.Find(id);
            return query;
        }

        public string DeleteImage(int id, string type)
        {
            Store image = db.Store.Find(id);
            var delete = string.Empty;
            switch (type)
            {
                case "Image":
                    delete = image.Image;
                    image.Image = string.Empty;
                    break;
                case "Image2":
                    delete = image.Image2;
                    image.Image2 = string.Empty;
                    break;
            }
            db.SaveChanges();
            return delete;
        }

        public void Delete(int id)
        {
            Store delete = db.Store.Find(id);
            db.Store.Remove(delete);
            db.SaveChanges();
        }
    }
}