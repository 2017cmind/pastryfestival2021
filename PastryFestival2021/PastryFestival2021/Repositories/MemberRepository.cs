﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PastryFestival2021.Models;

namespace PastryFestival2021.Repositories
{
    public class MemberRepository
    {
        private PastryFestival2021DBEntities db = new PastryFestival2021DBEntities();

        public void Update(Member newMember)
        {
            Member member = db.Member.Find(newMember.ID);
            member.Email = newMember.Email;
            member.Name = newMember.Name;
            member.Phone = newMember.Phone;
            member.UpdateTime = DateTime.Now;
            db.SaveChanges();
        }
        public int Insert(Member data)
        {
            data.UpdateTime = DateTime.Now;
            data.CreateTime = DateTime.Now;
            db.Member.Add(data);
            db.SaveChanges();
            return data.ID;
        }

        #region 前台     

        public Member SocialLogin(string userId)
        {
            Member member = null;
            member = db.Member.SingleOrDefault(p => p.FBId == userId);

            return member;
        }
        #endregion
    }
}