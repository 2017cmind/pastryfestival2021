﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PastryFestival2021.Models;

namespace PastryFestival2021.Repositories
{
    public class NewsRepository
    {
        private PastryFestival2021DBEntities db = new PastryFestival2021DBEntities();

        public void Update(News oldData)
        {
            News data = db.News.Find(oldData.ID);
            data.Title = oldData.Title;
            data.Content = oldData.Content;
            data.OnlineTime = oldData.OnlineTime;
            data.Image = oldData.Image;
            data.Updater = oldData.Updater;
            data.UpdateTime = DateTime.Now;
            db.SaveChanges();
        }

        public int Insert(News data)
        {
            data.UpdateTime = DateTime.Now;
            data.CreateTime = DateTime.Now;
            db.News.Add(data);
            db.SaveChanges();
            return data.ID;
        }

        public IQueryable<News> Query(string title)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(p => p.Title.Contains(title));
            }
            return query;
        }

        public IQueryable<News> GetAll()
        {
            var query = db.News;
            return query;
        }

        public News GetById(int id)
        {
            var query = db.News.Find(id);
            return query;
        }

        public string DeleteImage(int id)
        {
            News image = db.News.Find(id);
            var delete = image.Image;
            image.Image = string.Empty;
            db.SaveChanges();
            return delete;
        }

        public void Delete(int id)
        {
            News delete = db.News.Find(id);
            db.News.Remove(delete);
            db.SaveChanges();
        }


        public IQueryable<News> List()
        {
            var query = GetAll().Where(p => p.OnlineTime <= DateTime.Now).OrderByDescending(p => p.OnlineTime);
            return query;
        }
    }
}