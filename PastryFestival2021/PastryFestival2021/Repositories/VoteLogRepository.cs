﻿using PastryFestival2021.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PastryFestival2021.Repositories
{
    public class VoteLogRepository
    {
        private PastryFestival2021DBEntities db = new PastryFestival2021DBEntities();

        public IQueryable<VoteLog> Query(int vid)
        {
            var query = db.VoteLog.AsQueryable();
            if (vid != 0)
            {
                query = query.Where(p => p.VoteId == vid);
            }       
            return query;
        }

        public IQueryable<VoteLog> GetAll()
        {
            var query = db.VoteLog;
            return query;
        }

        public void Delete(int id)
        {
            VoteLog delete = db.VoteLog.Find(id);
            db.VoteLog.Remove(delete);
            db.SaveChanges();
        }


        public int Insert(int vid, string fid, string name)
        {
            var data = new VoteLog();
            data.FBId = fid;
            data.VoteId = vid;
            data.Name = name;
            DateTime now = DateTime.Now;
            data.CreateTime = now;
            db.VoteLog.Add(data);
            db.SaveChanges();
            return data.ID;
        }
        public VoteLog GetVoteLog(string fid)
        {
            DateTime now = DateTime.Today;

            var query = db.VoteLog.Where(p => p.FBId== fid && DbFunctions.TruncateTime(p.CreateTime) == now).SingleOrDefault();
            return query;
        }
    }
}